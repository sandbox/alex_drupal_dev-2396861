<?php
/**
 * @file
 * newsarticle.features.inc
 */

/**
 * Implements hook_node_info().
 */
function newsarticle_node_info() {
  $items = array(
    'news_article' => array(
      'name' => t('News Article'),
      'base' => 'node_content',
      'description' => t('Add a new news article to the site.'),
      'has_title' => '1',
      'title_label' => t('Article Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
